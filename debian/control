Source: libsoil
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Christoph Egger <christoph@debian.org>
Build-Depends:
 debhelper (>= 10),
 libgl-dev,
 mesa-common-dev,
 quilt
Rules-Requires-Root: binary-targets
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/games-team/libsoil.git
Vcs-Browser: https://salsa.debian.org/games-team/libsoil
Section: libs
Homepage: http://www.lonesock.net/soil.html

Package: libsoil-dev
Section: libdevel
Architecture: any
Depends:
 ${misc:Depends},
 libsoil1 (= ${binary:Version})
Description: Simple OpenGL Image Library - development files
 SOIL is a tiny C library used primarily for uploading textures into OpenGL.
 It supports loading BMP, PNG, JPG, TGA, DDS, PSD and HDR files as well as
 saving into TGA, BMP and DDS Files.
 .
 It is also able to perform common functions needed in loading OpenGL textures.
 .
 This package contains everything needed to develop software using libsoil.

Package: libsoil1
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: Simple OpenGL Image Library
 SOIL is a tiny C library used primarily for uploading textures into OpenGL.
 It supports loading BMP, PNG, JPG, TGA, DDS, PSD and HDR files as well as
 saving into TGA, BMP and DDS Files.
 .
 It is also able to perform common functions needed in loading OpenGL textures.
 .
 This is the shared library potentially needed for some other software to work.
